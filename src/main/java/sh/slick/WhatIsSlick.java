package sh.slick;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import sh.pres.AbstractPage;
import sh.pres.comment.CommentsJFrame;
import sh.pres.components.Footer;
import sh.pres.components.Text;
import sh.pres.components.TextList;
import sh.pres.styles.Style;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class WhatIsSlick extends AbstractPage {

	@Override
	public void enter(GameContainer container, StateBasedGame game) throws SlickException {
		super.enter(container, game);
		super.removeAllElement();
		super.addElement(new Text("Slick 2D", "title"));
		super.addElement(new Footer(this, Pages.class));
		super.addElement(new TextList("Lib de Jeu", "  Affichage, I/O, Son",
				"  LWJGL, OpenAl, JInput", "  Linux, OsX, Windows", " ", "Ce soir :",
				"  Boucle, Animation, Carte, Collision"));
		super.applyStyle(Style.get());
		CommentsJFrame.comments("qu'est ce que slick", "- lib de jeux openSource",
				"- s'appuye sur OpenGL/OpenAl et JInput via LWJGL",
				"- gere ce qui est necessaire pour faire un jeux 2D",
				"- de bas niveau ou de haute niveau", "- disponible dur les plateforme desktop",
				"- [TRANSI] voyons ce qu'est un jeux");
	}

	@Override
	public int getID() {
		return Pages.WHAT_IS_SLICK.ordinal();
	}

}
