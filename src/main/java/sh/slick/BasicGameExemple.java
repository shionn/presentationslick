/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package sh.slick;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import sh.pres.AbstractPage;
import sh.pres.comment.CommentsJFrame;
import sh.pres.components.Footer;
import sh.pres.components.Image;
import sh.pres.components.Text;
import sh.pres.styles.Style;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class BasicGameExemple extends AbstractPage {

	private int step = 0;
	private Image mainLoop;
	private Image gamecontainer;

	@Override
	public void enter(GameContainer container, StateBasedGame game) throws SlickException {
		super.enter(container, game);
		this.step = 0;
		super.removeAllElement();
		super.addElement(new Text("Premiere application", "title"));
		super.addElement(new Footer(this, Pages.class));
		mainLoop = super.addElement(new Image("basic-game-exemple.png", "code"));
		super.applyStyle(Style.get());
		CommentsJFrame.comments("- class abstraite BasicGame",
				"- Ca force l'implémentation des étape du jeux",
				"- pour lancer la boucle un utilise un Contener",
				"- different types, application standelone, canvas, Applet",
				"- [SPACE] prenons le plus simple ", "- [SPACE] voici le resultat",
				"- [TRANSI] remplissons tous ca"

		);
	}

	@Override
	public int getID() {
		return Pages.BASIC_GAME_EXEMPLE.ordinal();
	}

	@Override
	protected void next() {
		switch (step++) {
		case 0:
			mainLoop.setBounds(2, 20, 56, 60);
			gamecontainer = addImg("app-container-exemple.png", "code");
			break;
		case 1:
			gamecontainer.setBounds(2, 75, 56, 20);
			addImg("basic-game-sc.png", "screenshots");
			break;
		default:
			super.next();
		}
	}

	private Image addImg(String file, String style) {
		try {
			return super.addElement(new Image(file, style), Style.get());
		} catch (SlickException e) {
			throw new RuntimeException(e);
		}
	}
}
