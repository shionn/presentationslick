/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package sh.slick;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import sh.pres.AbstractPage;
import sh.pres.comment.CommentsJFrame;
import sh.pres.components.Text;
import sh.pres.styles.Style;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class TitlePage extends AbstractPage {

	private static final String[] GEEK_CODE = {
			"GCS/O d- s+:+ a C++ UL P L+++ E--- W++ N K- w-- M-",
			"PE--- Y- PGP- t+ 5 X R+ !tv b+ D+ G- e+++ h+ r++ y+" };

	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		super.init(container, game);
		super.addElement(new Text("Slick 2D / Java", "main-title"));
		super.addElement(new Text("Quick game making.", "sub-main-title"));
		super.addElement(new Text("KEPKA Ludovic", "author"));
		super.addElement(new Text("Shionn@gmail.com", "mail"));
		super.addElement(new Text(GEEK_CODE[0], "gc1"));
		super.addElement(new Text(GEEK_CODE[1], "gc2"));
		super.applyStyle(Style.get());
	}

	@Override
	public void enter(GameContainer container, StateBasedGame game) throws SlickException {
		super.enter(container, game);
		CommentsJFrame.comments("Intro", "- je m'appel Ludovic KEPKA",
				"- Je travail au CA à archamps", "- geek leger :] j'aime le code",
				"- mail : shionn@gmail.com", "[TRANSI] voila pour moi, slick est ...");
		// super.switchTo(Pages.MOVE_CODE.ordinal());
	}

	@Override
	public void leave(GameContainer container, StateBasedGame game) throws SlickException {
		super.leave(container, game);
		CommentsJFrame.startChrono();
	}

	@Override
	public int getID() {
		return Pages.TITLE.ordinal();
	}

	@Override
	protected void previous() {
		// ne rien faire
	}

}
