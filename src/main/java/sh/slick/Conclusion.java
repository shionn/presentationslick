package sh.slick;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import sh.pres.AbstractPage;
import sh.pres.comment.CommentsJFrame;
import sh.pres.components.Footer;
import sh.pres.components.Text;
import sh.pres.components.TextList;
import sh.pres.styles.Style;

public class Conclusion extends AbstractPage {

	private TextList text;
	private int step;

	@Override
	public void enter(GameContainer container, StateBasedGame game) throws SlickException {
		super.enter(container, game);
		step = 0;
		super.removeAllElement();
		super.addElement(new Text("Conclusion", "title"));
		super.addElement(new Footer(this, Pages.class));
		text = super.addElement(new TextList("Lib de jeu simple", "  idéale pour un premier jeu"));
		super.applyStyle(Style.get());
		CommentsJFrame.comments("Conclusion",
				"- Lib de jeu simple idéale pour s'innitié au techno du jeux.",
				"- Prendre ces marques", "- [Space] malheureusement abandonné par son auteur",
				"- Mais je vous encourage à essayer, mais ne chercher pas à refaire le monde",
				"- [space] quelque ressource");

	}

	@Override
	protected void next() {
		switch (step++) {
		case 0:
			text.add("  Abandonnée par son auteur.");
			break;
		case 1:
			text.add("  ", "  slick.ninjacave.com", "  opengameart.org",
					"  shionn.org/ht-geneve-slick2d");
		default:
			break;
		}
	}

	@Override
	public int getID() {
		return Pages.CONCLUSION.ordinal();
	}

}
