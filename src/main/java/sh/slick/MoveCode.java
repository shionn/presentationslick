/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package sh.slick;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import sh.pres.AbstractPage;
import sh.pres.comment.CommentsJFrame;
import sh.pres.components.Footer;
import sh.pres.components.Image;
import sh.pres.components.Text;
import sh.pres.styles.Style;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class MoveCode extends AbstractPage {

	private int step;
	private Image current;

	@Override
	public void enter(GameContainer container, StateBasedGame game) throws SlickException {
		super.enter(container, game);
		step = 0;
		super.removeAllElement();
		super.addElement(new Text("Déplacement", "title"));
		current = super.addElement(new Image("character-all-anim.png", "code"));
		super.addElement(new Footer(this, Pages.class));
		super.applyStyle(Style.get());
		CommentsJFrame.comments("Let's go !", "[TRANSI] voyons le résultat");
	}

	@Override
	protected void next() {
		switch (step++) {
		case 0:
			current.setBounds(2, 20, 35, 20);
			current = addImage("move-exemple-definition.png");
			break;
		case 1:
			current.setBounds(46, 20, 52, 20);
			current = addImage("move-exemple-loading.png");
			break;
		case 2:
			current.setBounds(2, 40, 40, 30);
			current = addImage("move-exemple-input.png");
			break;
		case 3:
			current.setBounds(46, 40, 52, 38);
			current = addImage("move-exemple-draw.png");
			break;
		case 4:
			current.setBounds(2, 70, 40, 26);
			current = addImage("move-exemple-update.png");
			break;
		default:
			super.next();
			break;
		}
	}

	private Image addImage(String img) {
		try {
			return super.addElement(new Image(img, "code"), Style.get());
		} catch (SlickException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public int getID() {
		return Pages.MOVE_CODE.ordinal();
	}

}
