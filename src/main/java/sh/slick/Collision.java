/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package sh.slick;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import sh.pres.AbstractPage;
import sh.pres.comment.CommentsJFrame;
import sh.pres.components.Footer;
import sh.pres.components.Text;
import sh.pres.components.TiledMapComponent;
import sh.pres.styles.Style;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class Collision extends AbstractPage {

	private int step;
	private TiledMapComponent map;

	@Override
	public void enter(GameContainer container, StateBasedGame game) throws SlickException {
		super.enter(container, game);
		step = 0;
		super.removeAllElement();
		super.addElement(new Text("Collision", "title"));
		map = super.addElement(new TiledMapComponent("map_collision.tmx"));
		super.addElement(new Footer(this, Pages.class));
		super.applyStyle(Style.get());
		CommentsJFrame.comments("Let's go !", "[TRANSI] Voyons le résultat");
	}

	@Override
	protected void next() {
		switch (step++) {
		case 0:
			map.addLayout(1);
			break;
		case 1:
			map.addLayout(2);
			break;
		case 2:
			map.addLayout(3);
			break;
		case 3:
			map.addLayout(4);
			break;
		default:
			super.next();
			break;
		}
	}

	@Override
	public int getID() {
		return Pages.COLLISION.ordinal();
	}

}
