/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package sh.slick;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.state.StateBasedGame;

import sh.pres.AbstractPage;
import sh.pres.comment.CommentsJFrame;
import sh.pres.components.Animation;
import sh.pres.components.Footer;
import sh.pres.components.Image;
import sh.pres.components.Text;
import sh.pres.styles.Style;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class AnimatedSpriteExemple extends AbstractPage {

	private int step;
	private Image image;
	private Image code;

	@Override
	public void enter(GameContainer container, StateBasedGame game) throws SlickException {
		super.enter(container, game);
		super.removeAllElement();
		this.step = 0;
		super.addElement(new Text("Une animation", "title"));
		super.addElement(new Footer(this, Pages.class));
		image = super.addElement(new Image("character.png", "code"));
		super.applyStyle(Style.get());
		CommentsJFrame.comments("- découpage du sprite pour faire l'animation de marche.",
				"- On se limite à certaine image", "[SPACE] voila comment on charge",
				"[SPACE] voila comment on affiche", "[TRANSI] le mettre sur carte");

	}

	@Override
	protected void next() {
		switch (step++) {
		case 0:
			step0();
			break;
		case 1:
			step1();
			break;
		default:
			super.next();
		}
	}

	private void step0() {
		image.setBounds(2, 20, 56, 40);
		try {
			code = super.addElement(new Image("sprite-animation-exemple.png", "code"), Style.get());
		} catch (SlickException e) {
			throw new RuntimeException(e);
		}
	}

	private void step1() {
		try {
			code.setBounds(2, 60, 56, 40);
			SpriteSheet sheet = new SpriteSheet("sprite/character.png", 576 / 9, 256 / 4);
			org.newdawn.slick.Animation animation = new org.newdawn.slick.Animation();
			for (int x = 1; x < 9; x++) {
				animation.addFrame(sheet.getSprite(x, 1), 100);
			}
			addElement(new Animation(animation), Style.get()).setBounds(60, 30, 38, 42);
		} catch (SlickException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public int getID() {
		return Pages.ANIMATED_SPRITE_EXEMPLE.ordinal();
	}

}
