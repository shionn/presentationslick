/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package sh.slick;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import sh.exemple.CollisionGameExemple;
import sh.pres.AbstractPage;
import sh.pres.comment.CommentsJFrame;
import sh.pres.components.Footer;
import sh.pres.components.GameComponent;
import sh.pres.components.Text;
import sh.pres.styles.Style;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class CollisionResult extends AbstractPage {

	@Override
	public void enter(GameContainer container, StateBasedGame game) throws SlickException {
		super.removeAllElement();
		super.enter(container, game);
		super.addElement(new Text("Collision", "title"));
		super.addElement(new Footer(this, Pages.class));
		super.addElement(new GameComponent(new CollisionGameExemple(), container));
		super.applyStyle(Style.get());
		CommentsJFrame.comments("Let's go !", "[TRANSI] c'est tous pour les exemples.");
	}

	@Override
	public int getID() {
		return Pages.COLLISION_RESULT.ordinal();
	}

}
