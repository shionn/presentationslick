package sh.slick;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import sh.pres.AbstractPage;
import sh.pres.comment.CommentsJFrame;
import sh.pres.components.Footer;
import sh.pres.components.Line;
import sh.pres.components.PolygonShape;
import sh.pres.components.Text;
import sh.pres.styles.Style;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class WhatIsAGame extends AbstractPage {

	@Override
	public void enter(GameContainer container, StateBasedGame game) throws SlickException {
		super.enter(container, game);
		super.removeAllElement();
		super.addElement(new Text("Jeux : boucle infinie", "title"));
		super.addElement(new Footer(this, Pages.class));
		super.addElement(new Line(35, 40, 35, 60));
		super.addElement(new Line(45, 70, 45, 75));
		super.addElement(new Line(45, 75, 70, 75));
		super.addElement(new Line(70, 75, 70, 25));
		super.addElement(new Line(70, 25, 35, 25));
		super.addElement(new Line(35, 25, 35, 30));
		super.addElement(new Line(25, 70, 25, 80));
		super.addElement(new PolygonShape(35, 30, "down-arrow"));
		super.addElement(new PolygonShape(35, 45, "down-arrow"));
		super.addElement(new PolygonShape(35, 60, "down-arrow"));
		super.addElement(new PolygonShape(25, 80, "down-arrow"));
		super.addElement(new Text("Affichage", "jeux.display"));
		super.addElement(new Text("I/O", "jeux.io"));
		super.addElement(new Text("Mise à jour", "jeux.update"));
		super.addElement(new Text("Fin de jeux", "jeux.end"));
		super.applyStyle(Style.get());
		CommentsJFrame.comments("qu'est ce qu'un jeux",
				"- une simple boucle infinie, depuis que le jeux existe c'est comme ca",
				"- Slick gere les boucles. il suffit d'implementer Game",
				"- [TRANSI] deux sous classe, voyons la plus simple");

	}

	@Override
	public int getID() {
		return Pages.WHAT_IS_GAME.ordinal();
	}

}
