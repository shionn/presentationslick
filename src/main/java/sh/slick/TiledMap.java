/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package sh.slick;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import sh.pres.AbstractPage;
import sh.pres.comment.CommentsJFrame;
import sh.pres.components.Footer;
import sh.pres.components.Image;
import sh.pres.components.Text;
import sh.pres.components.TextList;
import sh.pres.styles.Style;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class TiledMap extends AbstractPage {

	private int step;
	private Image image;

	@Override
	public void enter(GameContainer container, StateBasedGame game) throws SlickException {
		super.enter(container, game);
		super.removeAllElement();
		this.step = 0;
		super.addElement(new Text("Une carte", "title"));
		super.addElement(new Footer(this, Pages.class));
		super.addElement(new TextList("Tiled map Editor", "  Libre et multi plateforme",
				"  Orthographic et 3D Isométrique", "  Objects et couches...",
				"  http://www.mapeditor.org/"));
		super.applyStyle(Style.get());
		CommentsJFrame.comments("Let's go !", "[TRANSI] on creer la carte ajoutons la au jeux");

	}

	@Override
	protected void next() {
		switch (step++) {
		case 0:
			image = addImg("tiled-isometric.png", "code");
			break;
		case 1:
			image.setBounds(5, 75, 20, 20);
			image = addImg("tiled-objects.png", "code");
			break;
		case 2:
			image.setBounds(40, 75, 20, 20);
			image = addImg("tiled-exemple.png", "code");
			break;
		case 3:
			image.setBounds(75, 75, 20, 20);
			break;
		default:
			super.next();
		}
	}

	private Image addImg(String file, String style) {
		try {
			return super.addElement(new Image(file, style), Style.get());
		} catch (SlickException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public int getID() {
		return Pages.TILED_MAP.ordinal();
	}

}
