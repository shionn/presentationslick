/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package sh.slick;

import sh.pres.AbstractPage;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public enum Pages {
	TITLE(TitlePage.class),
	WHAT_IS_GAME(WhatIsAGame.class),
	WHAT_IS_SLICK(WhatIsSlick.class),
	BASIC_GAME_EXEMPLE(BasicGameExemple.class),
	ANIMATED_SPRITE_EXEMPLE(AnimatedSpriteExemple.class),
	TILED_MAP(TiledMap.class),
	TILED_MAP_CODE(TiledMapCode.class),
	TILED_MAP_RESULT(TiledMapResult.class),
	MOVE_CODE(MoveCode.class),
	MOVE_RESULT(MoveResult.class),
	COLLISION(Collision.class),
	COLLISION_RESULT(CollisionResult.class),
	DO_MORE(DoMore.class),
	CONCLUSION(Conclusion.class);
	private final Class<? extends AbstractPage> source;

	private Pages(Class<? extends AbstractPage> source) {
		this.source = source;
	}

	public Class<? extends AbstractPage> getSource() {
		return source;
	}
}
