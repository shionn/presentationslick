package sh.slick;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import sh.pres.AbstractPage;
import sh.pres.comment.CommentsJFrame;
import sh.pres.components.Footer;
import sh.pres.components.Text;
import sh.pres.components.TextList;
import sh.pres.styles.Style;

public class DoMore extends AbstractPage {

	@Override
	public void enter(GameContainer container, StateBasedGame game) throws SlickException {
		super.removeAllElement();
		super.addElement(new Text("Pour aller plus loin", "title"));
		super.addElement(new Footer(this, Pages.class));
		super.addElement(new TextList("Gestion des polices", "Gestion du son", "",
				"Interface (HUD) : TWL, Nifty", "Phisyque : JBox 2D", "Entitée : Artemis",
				"Reseau : RedDwarf"));
		super.applyStyle(Style.get());
		CommentsJFrame.comments("Let's go !");
	}

	@Override
	public int getID() {
		return Pages.DO_MORE.ordinal();
	}

}
