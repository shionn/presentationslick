/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package sh.exemple;

import org.newdawn.slick.Animation;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.tiled.TiledMap;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class CollisionGameExemple extends BasicGame {
    private GameContainer container;
    private Animation[] animations;
    private int direction = 2;
    private boolean moving = false;
    private float x = 300;
    private float y = 200;
    private TiledMap map;

    public CollisionGameExemple() {
        super("CollisionGameExemple");
    }

    @Override
    public void init(GameContainer container) throws SlickException {
        this.container = container;
        map = new TiledMap("map/map_collision.tmx");
        SpriteSheet spriteSheet = new SpriteSheet("sprite/character.png", 576 / 9, 256 / 4);
        animations = new Animation[8];
        animations[0] = loadAnimation(spriteSheet, 0, 1, 0);
        animations[1] = loadAnimation(spriteSheet, 0, 1, 1);
        animations[2] = loadAnimation(spriteSheet, 0, 1, 2);
        animations[3] = loadAnimation(spriteSheet, 0, 1, 3);
        animations[4] = loadAnimation(spriteSheet, 1, 9, 0);
        animations[5] = loadAnimation(spriteSheet, 1, 9, 1);
        animations[6] = loadAnimation(spriteSheet, 1, 9, 2);
        animations[7] = loadAnimation(spriteSheet, 1, 9, 3);
    }

    public Animation loadAnimation(SpriteSheet spriteSheet, int startX, int endX, int y) {
        Animation animation = new Animation();
        for (int x = startX; x < endX; x++) {
            animation.addFrame(spriteSheet.getSprite(x, y), 100);
        }
        return animation;
    }

    @Override
    public void render(GameContainer container, Graphics g) throws SlickException {
        map.render(0, 0, 0);
        map.render(0, 0, 1);
        g.drawAnimation(getCurrentAnimation(), x - 32, y - 64);
        map.render(0, 0, 2);
        map.render(0, 0, 3);
    }

    private Animation getCurrentAnimation() {
        int step = direction;
        if (moving) {
            step += 4;
        }
        return animations[step];
    }

    @Override
    public void update(GameContainer container, int delta) throws SlickException {
        if (moving) {
            float fy = getFuturY(delta);
            float fx = getFuturX(delta);
            if (!collision(fx, fy)) {
                x = fx;
                y = fy;
            }
        }
    }

    private boolean collision(float fx, float fy) {
        int cx = (int) fx / map.getTileWidth();
        int cy = (int) fy / map.getTileHeight();
        Image tile = map.getTileImage(cx, cy, 4);
        return tile != null;
    }

    private float getFuturX(int delta) {
        switch (direction) {
        case 1:
            return x - .1f * delta;
        case 3:
            return x + .1f * delta;
        default:
            return x;
        }
    }

    private float getFuturY(int delta) {
        switch (direction) {
        case 0:
            return y - .1f * delta;
        case 2:
            return y + .1f * delta;
        default:
            return y;
        }
    }

    @Override
    public void keyReleased(int key, char c) {
        moving = false;
        if (Input.KEY_ESCAPE == key) {
            container.exit();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.newdawn.slick.BasicGame#keyPressed(int, char)
     */
    @Override
    public void keyPressed(int key, char c) {
        if (Input.KEY_UP == key) {
            moving = true;
            direction = 0;
        } else if (Input.KEY_LEFT == key) {
            moving = true;
            direction = 1;
        } else if (Input.KEY_DOWN == key) {
            moving = true;
            direction = 2;
        } else if (Input.KEY_RIGHT == key) {
            moving = true;
            direction = 3;
        }
    }

}
