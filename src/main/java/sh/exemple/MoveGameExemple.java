/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package sh.exemple;

import org.newdawn.slick.Animation;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.tiled.TiledMap;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class MoveGameExemple extends BasicGame {
    private GameContainer container;
    private TiledMap map;
    private Animation[] animations;
    private int direction = 2;
    private boolean moving = false;
    private float x = 300, y = 200;

    public MoveGameExemple() {
        super("MoveGameExemple");
    }

    @Override
    public void init(GameContainer container) throws SlickException {
        this.container = container;
        map = new TiledMap("map/map.tmx");
        SpriteSheet spriteSheet = new SpriteSheet("sprite/character.png", 576 / 9, 256 / 4);
        animations = new Animation[8];
        animations[0] = loadAnimation(spriteSheet, 0, 1, 0);
        animations[1] = loadAnimation(spriteSheet, 0, 1, 1);
        animations[2] = loadAnimation(spriteSheet, 0, 1, 2);
        animations[3] = loadAnimation(spriteSheet, 0, 1, 3);
        animations[4] = loadAnimation(spriteSheet, 1, 9, 0);
        animations[5] = loadAnimation(spriteSheet, 1, 9, 1);
        animations[6] = loadAnimation(spriteSheet, 1, 9, 2);
        animations[7] = loadAnimation(spriteSheet, 1, 9, 3);
    }

    public Animation loadAnimation(SpriteSheet spriteSheet, int startX, int endX, int y) {
        Animation animation = new Animation();
        for (int x = startX; x < endX; x++) {
            animation.addFrame(spriteSheet.getSprite(x, y), 100);
        }
        return animation;
    }

    @Override
    public void render(GameContainer container, Graphics g) throws SlickException {
        map.render(0, 0);
        g.drawAnimation(getCurrentAnimation(), x, y);
    }

    private Animation getCurrentAnimation() {
        int step = direction;
        if (moving) {
            step += 4;
        }
        return animations[step];
    }

    @Override
    public void update(GameContainer container, int delta) throws SlickException {
        if (moving) {
            switch (direction) {
            case 0:
                y -= .1f * delta;
                break;
            case 1:
                x -= .1f * delta;
                break;
            case 2:
                y += .1f * delta;
                break;
            case 3:
                x += .1f * delta;
                break;
            }
        }
    }

    @Override
    public void keyReleased(int key, char c) {
        moving = false;
        if (Input.KEY_ESCAPE == key) {
            container.exit();
        }
    }

    @Override
    public void keyPressed(int key, char c) {
        switch (key) {
        case Input.KEY_UP:
            direction = 0;
            moving = true;
            break;
        case Input.KEY_LEFT:
            direction = 1;
            moving = true;
            break;
        case Input.KEY_DOWN:
            direction = 2;
            moving = true;
            break;
        case Input.KEY_RIGHT:
            direction = 3;
            moving = true;
            break;
        }
    }

}
