/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package sh.exemple;

import org.newdawn.slick.Animation;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class SpriteGameExemple extends BasicGame {
    private GameContainer container;
    private Animation animation;

    public SpriteGameExemple() {
        super("SpriteGameExemple");
    }

    @Override
    public void init(GameContainer container) throws SlickException {
        this.container = container;
        SpriteSheet spriteSheet = new SpriteSheet("sprite/character.png", 576 / 9, 256 / 4);
        animation = new Animation();
        for (int x = 1; x < 9; x++) {
            animation.addFrame(spriteSheet.getSprite(x, 1), 100);
        }
    }

    @Override
    public void render(GameContainer container, Graphics g) throws SlickException {
        g.drawAnimation(animation, 300, 200);
    }

    @Override
    public void update(GameContainer container, int delta) throws SlickException {
        // déplacer, tourner, etc...
    }

    @Override
    public void keyReleased(int key, char c) {
        if (Input.KEY_ESCAPE == key) {
            container.exit();
        }
    }

}
