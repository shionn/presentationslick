/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package sh.pres.styles;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import sh.pres.components.Component;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class Style {

    private static Style INSTANCE = new Style();

    static {
        try {
            INSTANCE.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Style get() {
        return INSTANCE;
    }

    private static final String DEFAULT = "default";
    private final Map<String, JsonNode> datas = new HashMap<String, JsonNode>();

    public Style() {
    }

    public void load() throws IOException {
        JsonNode root = new ObjectMapper().readTree(this.getClass().getClassLoader()
                .getResource("styles/style.json"));
        Iterator<Entry<String, JsonNode>> iterator = root.getFields();
        while (iterator.hasNext()) {
            Entry<String, JsonNode> child = iterator.next();
            datas.put(child.getKey(), child.getValue());
        }
    }

    public List<JsonNode> list(Component component) {
        ArrayList<JsonNode> list = new ArrayList<JsonNode>();
        addIfExist(list, DEFAULT);
        addIfExist(list, component.getClass().getName());
        addIfExist(list, component.getStyleName());
        return list;
    }

    private void addIfExist(ArrayList<JsonNode> list, String name) {
        JsonNode e = datas.get(name);
        if (e != null) {
            list.add(e);
        }
    }

}
