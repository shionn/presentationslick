/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package sh.pres.fonts;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.font.effects.Effect;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class FontLoader {

    private static final Map<FontLoader, UnicodeFont> fonts = new HashMap<FontLoader, UnicodeFont>();

    private String font = "arial.ttf";
    private int size = 42;
    private boolean bold = false;
    private boolean italic = false;
    private final List<Effect> effects = new ArrayList<Effect>();

    public FontLoader() {
    }

    public FontLoader(String font) {
        font(font);
    }

    public FontLoader font(String font) {
        this.font = font;
        return this;
    }

    public FontLoader size(int size) {
        this.size = size;
        return this;

    }

    public FontLoader bold() {
        this.bold = true;
        return this;
    }

    public FontLoader italic() {
        this.italic = true;
        return this;
    }

    public FontLoader color(Color color) {
        effects.add(new ColorEffect(color));
        return this;
    }

    public FontLoader color(String color) {
        return color(new Color(Integer.parseInt(color, 16)));
    }

    public FontLoader effect(Effect effect) {
        this.effects.add(effect);
        return this;
    }

    public UnicodeFont get() throws SlickException {
        UnicodeFont unicodeFont = fonts.get(this);
        if (unicodeFont == null) {
            unicodeFont = load();
            fonts.put(this, unicodeFont);
        }
        return unicodeFont;
    }

    @SuppressWarnings("unchecked")
    private UnicodeFont load() throws SlickException {
        UnicodeFont unicodeFont = new UnicodeFont("fonts/" + font, size, bold, italic);
        for (Effect effect : effects) {
            unicodeFont.getEffects().add(effect);
        }
        unicodeFont.addAsciiGlyphs();
        unicodeFont.loadGlyphs();
        return unicodeFont;
    }

    @Override
    public int hashCode() {
        String key = font + ',' + size + ',' + bold + ',' + italic;
        for (Effect effect : effects) {
            key += ',' + effect.getClass().getSimpleName();
            if (effect instanceof ColorEffect) {
                key += ',' + ((ColorEffect) effect).getColor().toString();
            } else {
                throw new RuntimeException("unknonw Effect : " + effect.getClass());
            }
        }
        return key.hashCode();
    }

}
