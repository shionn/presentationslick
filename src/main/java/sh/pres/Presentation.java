/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package sh.pres;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import sh.slick.Pages;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class Presentation extends StateBasedGame {

    private static final String TITLE = "Présentation Slick En Slick";

    public static void main(String[] args) throws SlickException {
        new Presentation().start();
    }

    public Presentation() {
        super(TITLE);
    }

    private void start() throws SlickException {
        AppGameContainer gameContainer = new AppGameContainer(this);
        gameContainer.setDisplayMode(800, 600, false);
        gameContainer.setAlwaysRender(true);
        gameContainer.setTitle(TITLE);
        gameContainer.setVSync(true);
        gameContainer.setForceExit(false);
        gameContainer.setMultiSample(0);
        gameContainer.start();
        gameContainer.setShowFPS(false);
    }

    @Override
    public void initStatesList(GameContainer arg0) throws SlickException {
        for (Pages page : Pages.values()) {
            try {
                addState(page.getSource().newInstance());
            } catch (Exception e) {
                throw new SlickException("Erreur pendant l'instanciation des pages", e);
            }
        }
    }
}
