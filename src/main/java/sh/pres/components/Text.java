/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package sh.pres.components;

import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import sh.pres.styles.SlickColorDeserializer;
import sh.pres.styles.SlickFontDeserializer;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class Text extends Component {

    public enum Align {
        LEFT,
        RIGHT,
        CENTER,
        TOP,
        BOTTOM;
    }

    @JsonDeserialize(using = SlickFontDeserializer.class)
    private Font font;
    private String text;
    private Align align = Align.LEFT;
    private Align valign;

    @JsonDeserialize(using = SlickColorDeserializer.class)
    private Color background;

    public Text(String text, String styleName) {
        this.text = text;
        setStyleName(styleName);
    }

    public Text(String text) {
        this.text = text;
    }

    @Override
    public void render(GameContainer container, Graphics g) {
        if (background != null) {
            g.setColor(background);
            g.fillRect(x(container), y(container), w(container), h(container));
        }
        super.renderBorder(container, g);
        float scale = h(container) / font.getLineHeight();
        g.scale(scale, scale);
        g.setFont(font);
        g.drawString(text, computeX(container, scale), y(container) / scale);
        g.resetTransform();
    }

    private float computeX(GameContainer container, float scale) {
        switch (align) {
        case LEFT:
            return x(container) / scale;
        case RIGHT:
            return x(container) / scale + w(container) / scale - font.getWidth(text);
        case CENTER:
        default:
            return x(container) / scale + w(container) / 2f / scale - font.getWidth(text) / 2f;
        }
    }

    public Font getFont() {
        return font;
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Align getAlign() {
        return align;
    }

    public void setAlign(Align align) {
        this.align = align;
    }

    public Align getValign() {
        return valign;
    }

    public void setValign(Align valign) {
        this.valign = valign;
    }

    public Color getBackground() {
        return background;
    }

    public void setBackground(Color background) {
        this.background = background;
    }

}
