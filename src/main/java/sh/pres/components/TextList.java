package sh.pres.components;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import sh.pres.styles.SlickFontDeserializer;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class TextList extends Component {
    @JsonDeserialize(using = SlickFontDeserializer.class)
    private Font font;
    private final List<String> texts = new ArrayList<String>();

    public TextList(String... texts) {
        add(texts);
    }

    @Override
    public void render(GameContainer container, Graphics g) {
        int decalage = 0;
        float scale = h(container) / font.getLineHeight();
        g.scale(scale, scale);
        g.setFont(font);
        for (String text : texts) {
            g.drawString(text, x(container) / scale, y(container) / scale + decalage);
            decalage += font.getLineHeight();
        }
        g.resetTransform();

    }

    public void add(String... texts) {
        for (String text : texts) {
            this.texts.add(text);
        }
    }
}
