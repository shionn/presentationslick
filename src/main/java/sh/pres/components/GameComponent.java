/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package sh.pres.components;

import org.lwjgl.input.Cursor;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.opengl.ImageData;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class GameComponent extends Component {

    private final BasicGame game;
    private final GameContainerHolder containerHolder;

    public GameComponent(BasicGame game, GameContainer container) throws SlickException {
        this.game = game;
        containerHolder = new GameContainerHolder(container);
        game.init(containerHolder);
    }

    @Override
    public void render(GameContainer container, Graphics g) throws SlickException {
        g.pushTransform();
        g.translate(x(container), y(container));
        g.scale(w(container) / 640f, h(container) / 480f);
        game.render(container, g);
        g.popTransform();
    }

    @Override
    public void update(int delta) throws SlickException {
        game.update(containerHolder, delta);
    }

    @Override
    public void keyPressed(int key, char c) {
        game.keyPressed(key, c);
    }

    @Override
    public void keyReleased(int key, char c) {
        game.keyReleased(key, c);
    }

    public class GameContainerHolder extends GameContainer {

        public GameContainerHolder(@SuppressWarnings("unused") GameContainer container) {
            super(null);
        }

        @Override
        public int getScreenWidth() {
            return 0;
        }

        @Override
        public int getScreenHeight() {
            return 0;
        }

        @Override
        public void setIcon(String ref) throws SlickException {
        }

        @Override
        public void setIcons(String[] refs) throws SlickException {
        }

        @Override
        public void setMouseCursor(String ref, int hotSpotX, int hotSpotY) throws SlickException {
        }

        @Override
        public void setMouseCursor(ImageData data, int hotSpotX, int hotSpotY)
                throws SlickException {
        }

        @Override
        public void setMouseCursor(Image image, int hotSpotX, int hotSpotY) throws SlickException {
        }

        @Override
        public void setMouseCursor(Cursor cursor, int hotSpotX, int hotSpotY) throws SlickException {
        }

        @Override
        public void setDefaultMouseCursor() {
        }

        @Override
        public void setMouseGrabbed(boolean grabbed) {
        }

        @Override
        public boolean isMouseGrabbed() {
            return false;
        }

        @Override
        public boolean hasFocus() {
            return false;
        }

        @Override
        public void exit() {
            // do nothing
        }
    }

}
