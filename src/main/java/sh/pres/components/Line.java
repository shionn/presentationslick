package sh.pres.components;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class Line extends Component {

    private int width;

    public Line(int x, int y, int w, int h) {
        setBounds(x, y, w, h);
    }

    public Line(int x, int y, int w, int h, String style) {
        this(x, y, w, h);
        setStyleName(style);
    }

    @Override
    public void render(GameContainer container, Graphics g) {
        g.setColor(getColor());
        g.setLineWidth(width);
        g.drawLine(x(container), y(container), w(container), h(container));
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

}
