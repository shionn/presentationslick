/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package sh.pres.components;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class Image extends Component {

    private final org.newdawn.slick.Image image;
    private boolean keepAr;

    public Image(String file, String style) throws SlickException {
        this(file);
        setStyleName(style);
    }

    public Image(String file) throws SlickException {
        image = new org.newdawn.slick.Image("img/" + file);
    }

    @Override
    public void render(GameContainer container, Graphics g) {
        if (keepAr) {
            renderKeepingAr(container);
        } else {
            image.draw(x(container), y(container), w(container), h(container));
        }
    }

    @Override
    public void release() {
        try {
            image.destroy();
        } catch (SlickException e) {
            throw new RuntimeException(e);
        }
    }

    private void renderKeepingAr(GameContainer container) {
        float imgRatio = (float)image.getWidth() / (float)image.getHeight();
        float displayRatio = w(container) / h(container);
        float scale = 1;
        if (imgRatio < displayRatio) {
            scale = h(container) / image.getHeight();
        } else {
            scale = w(container) / image.getWidth();
        }

        image.getScaledCopy(scale).drawCentered(x(container) + w(container) / 2,
                y(container) + h(container) / 2);
    }

    public void setKeepAr(boolean keepAr) {
        this.keepAr = keepAr;
    }

    public boolean isKeepAr() {
        return keepAr;
    }

}
