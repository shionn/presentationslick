package sh.pres.components;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class DebugGrid extends Component {

    private static final int GRID_SPACE = 10;
    private static final int GRID_COUNT = 10;

    public DebugGrid() {
        setBounds(0, 0, 100, 100);
    }

    @Override
    public void render(GameContainer container, Graphics g) {
        g.setColor(Color.pink);
        g.setLineWidth(1);
        for (int l = 1; l < GRID_COUNT; l++) {
            g.drawLine(0, dy(l * GRID_SPACE, container), w(container),
                    dy(l * GRID_SPACE, container));
        }
        for (int c = 1; c < GRID_COUNT; c++) {
            g.drawLine(dx(c * GRID_SPACE, container), 0, dx(c * GRID_SPACE, container),
                    h(container));
        }
    }

}
