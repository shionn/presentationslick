/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package sh.pres.components;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class TiledMapComponent extends Component {

    private final TiledMap map;
    private final List<Integer> layouts;

    public TiledMapComponent(String map) throws SlickException {
        this.layouts = new ArrayList<Integer>();
        this.map = new TiledMap("map/" + map);
        addLayout(0);
    }

    @Override
    public void render(GameContainer container, Graphics g) throws SlickException {
        g.pushTransform();
        g.translate(x(container), y(container));
        g.scale(w(container) / (map.getWidth() * map.getTileWidth()),
                h(container) / (map.getHeight() * map.getTileHeight()));
        for (int layout : layouts) {
            map.render(0, 0, layout);
        }
        g.popTransform();
    }

    public void addLayout(int layout) {
        layouts.add(layout);
    }
}
