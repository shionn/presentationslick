/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package sh.pres.components;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class Animation extends Component {

    private final org.newdawn.slick.Animation animation;

    public Animation(org.newdawn.slick.Animation animation, String style) {
        this.animation = animation;
        setStyleName(style);
    }

    public Animation(org.newdawn.slick.Animation animation) {
        this.animation = animation;
    }

    @Override
    public void render(GameContainer container, Graphics g) {
        animation.draw(x(container), y(container), w(container), h(container));
    }

}
