/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package sh.pres.components;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import sh.pres.styles.SlickColorDeserializer;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class Component {

    @JsonDeserialize(using = SlickColorDeserializer.class)
    private Color color = Color.lightGray;
    private int x;
    private int y;
    private int w;
    private int h;
    private String styleName;

    private Border border;

    public abstract void render(GameContainer container, Graphics g) throws SlickException;

    public void release() {

    }

    public void update(@SuppressWarnings("unused") int delta) throws SlickException {
    }

    public void keyReleased(@SuppressWarnings("unused") int key, @SuppressWarnings("unused") char c) {
    }

    public void keyPressed(@SuppressWarnings("unused") int key, @SuppressWarnings("unused") char c) {
    }

    public void renderBorder(GameContainer container, Graphics g) {
        if (getBorder() != null) {
            g.setColor(getBorder().getColor());
            g.setLineWidth(getBorder().getWidth()[0]);
            g.drawLine(x(container), y(container), dx(w, container), y(container));
            g.setLineWidth(getBorder().getWidth()[1]);
            g.drawLine(dx(w, container), y(container), dx(w, container), dy(h, container));
            g.setLineWidth(getBorder().getWidth()[2]);
            g.drawLine(x(container), dy(h, container), dx(w, container), dy(h, container));
            g.setLineWidth(getBorder().getWidth()[3]);
            g.drawLine(x(container), y(container), x(container), dy(h, container));
        }
    }

    float x(GameContainer container) {
        return x * container.getWidth() / 100f;
    }

    float dx(float dx, GameContainer container) {
        return (x + dx) * container.getWidth() / 100f;
    }

    float y(GameContainer container) {
        return y * container.getHeight() / 100f;
    }

    float dy(float dy, GameContainer container) {
        return (y + dy) * container.getHeight() / 100f;
    }

    float w(GameContainer container) {
        return (float)w * container.getWidth() / 100f;
    }

    float h(GameContainer container) {
        return (float)h * container.getHeight() / 100f;
    }

    public void setBounds(int x, int y, int w, int h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    public void setWH(int w, int h) {
        setW(w);
        setH(h);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getW() {
        return w;
    }

    public void setW(int w) {
        this.w = w;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    public String getStyleName() {
        return styleName;
    }

    public void setStyleName(String styleName) {
        this.styleName = styleName;
    }

    public Border getBorder() {
        return border;
    }

    public void setBorder(Border border) {
        this.border = border;
    }

    public float scale(int s, GameContainer container) {
        return s * container.getWidth() / 100f;
    }

}
