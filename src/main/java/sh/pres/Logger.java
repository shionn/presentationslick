/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
package sh.pres;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class Logger {

    private final String name;

    public Logger(Class<?> clazz) {
        this.name = clazz.getSimpleName();
    }

    private void log(String level, String message) {
        System.out.println(name + ' ' + level + " : " + message);
    }

    public void debug(String message) {
        log("DEBUG", message);
    }

    public void info(String message) {
        log("INFO", message);
    }

    public void grave(String message) {
        log("GRAVE", message);
    }

    public void info(String message, Exception e) {
        StringWriter w = exceptionToString(e);
        info(message + '\n' + w);
    }

    public void grave(String message, Exception e) {
        StringWriter w = exceptionToString(e);
        grave(message + '\n' + w);
    }

    private StringWriter exceptionToString(Exception e) {
        StringWriter w = new StringWriter();
        e.printStackTrace(new PrintWriter(w));
        return w;
    }

}
