/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package sh.pres;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import sh.pres.comment.CommentsJFrame;
import sh.pres.components.Background;
import sh.pres.components.Component;
import sh.pres.styles.Style;
import sh.pres.styles.StyleLoader;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public abstract class AbstractPage extends BasicGameState {

	private static final List<Integer> NEXT_KEYS = Arrays.asList(Input.KEY_SPACE, Input.KEY_ENTER);
	private GameContainer container;

	private final List<Component> elements = new ArrayList<Component>();
	private Component background = new Background();
	private StateBasedGame game;
	private boolean takingScreenShot;

	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		this.container = container;
		this.game = game;
	}

	public void applyStyle(Style style) throws SlickException {
		StyleLoader loader = new StyleLoader(style);
		try {
			if (background != null) {
				loader.load(background);
			}
			for (Component element : elements) {
				loader.load(element);
			}
		} catch (IOException e) {
			throw new SlickException("can't apply style", e);
		}
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g)
			throws SlickException {
		if (background != null) {
			background.render(container, g);
		}
		for (Component element : elements) {
			element.render(container, g);
		}
		if (takingScreenShot) {
			takeScreenShot(container, g);
		}
	}

	@Override
	public void enter(GameContainer container, StateBasedGame game) throws SlickException {
		super.enter(container, game);
		takingScreenShot = true;
	}

	public void updateScreenShot() {
		takingScreenShot = true;
	}

	private void takeScreenShot(GameContainer container, Graphics g) throws SlickException {
		takingScreenShot = false;
		CommentsJFrame.takeScreenShot(container, g);
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {
		for (Component element : elements) {
			element.update(delta);
		}
	}

	@Override
	public void keyReleased(int key, char c) {
		for (Component element : elements) {
			element.keyReleased(key, c);
		}
		if (Input.KEY_F12 == key) {
			toggleFullScreen();
		} else if (NEXT_KEYS.contains(key)) {
			next();
		} else if (Input.KEY_BACK == key) {
			previous();
		}
	}

	@Override
	public void keyPressed(int key, char c) {
		for (Component element : elements) {
			element.keyPressed(key, c);
		}
	}

	private void toggleFullScreen() {
		try {
			AppGameContainer app = (AppGameContainer) container;
			if (app.isFullscreen()) {
				app.setDisplayMode(800, 600, false);
			} else {
				app.setDisplayMode(app.getScreenWidth(), app.getScreenHeight(), true);
			}
		} catch (SlickException e) {
			throw new RuntimeException(e);
		}
	}

	protected void next() {
		switchTo(getID() + 1);
	}

	protected void previous() {
		switchTo(getID() - 1);
	}

	public void switchTo(int state) {
		game.enterState(state);
	}

	public void setBackground(Component background) {
		this.background = background;
	}

	public <T extends Component> T addElement(T element) {
		this.elements.add(element);
		return element;
	}

	public <T extends Component> T addElement(T element, Style style) throws SlickException {
		try {
			new StyleLoader(style).load(element);
			return addElement(element);
		} catch (IOException e) {
			throw new SlickException("can't apply style", e);
		}
	}

	public void removeAllElement() {
		for (Component element : elements) {
			element.release();
		}
		this.elements.clear();
	}

}
