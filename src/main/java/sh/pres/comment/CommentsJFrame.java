package sh.pres.comment;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.TextArea;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.imageout.ImageOut;

public class CommentsJFrame extends JFrame {
	private static final String TITLE = "Commentaires ";
	private static final long serialVersionUID = 1702127200630870017L;
	private static final CommentsJFrame INSTANCE = null; // new
															// CommentsJFrame();
	private static boolean enable = INSTANCE != null;
	private TextArea comments;
	private Chrono chrono;
	private JLabel image;

	private CommentsJFrame() {
		super(TITLE);
		chrono = new Chrono(this);
		getContentPane().setLayout(new BorderLayout());
		comments = new TextArea();
		image = new JLabel();
		getContentPane().add(image, BorderLayout.CENTER);
		getContentPane().add(comments, BorderLayout.SOUTH);
		setSize(640, 480);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(true);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				chrono.exit();
				super.windowClosing(e);
			}
		});
	}

	public static void comments(String... texts) {
		if (enable) {
			get().comments.setText("");
			for (String text : texts) {
				get().comments.append(text + '\n');
			}
		}
	}

	private static CommentsJFrame get() {
		return INSTANCE;
	}

	public static void startChrono() {
		if (enable) {
			get().chrono.start();
		}
	}

	private void display(BufferedImage bufferedImage) {
		Image scaled = bufferedImage.getScaledInstance(image.getWidth(), image.getHeight(),
				BufferedImage.TYPE_INT_RGB);
		ImageIcon imageIcon = new ImageIcon(scaled);
		image.setIcon(imageIcon);
		bufferedImage.flush();
	}

	public static void takeScreenShot(GameContainer container, Graphics g) throws SlickException {
		if (enable) {
			org.newdawn.slick.Image target = new org.newdawn.slick.Image(container.getWidth(),
					container.getHeight());
			g.copyArea(target, 0, 0);
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			ImageOut.write(target, "BMP", os);
			target.destroy();
			try {
				BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(os
						.toByteArray()));
				get().display(bufferedImage);
			} catch (IOException e) {
				throw new SlickException("taking Screenshot", e);
			}
		}
	}

}
