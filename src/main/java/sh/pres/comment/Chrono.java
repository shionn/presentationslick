package sh.pres.comment;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.JFrame;

public class Chrono implements Runnable {

    private JFrame frame;
    private String title;
    private ScheduledExecutorService executor;
    private long start;

    public Chrono(JFrame frame) {
        this.frame = frame;
        this.title = frame.getTitle();
        executor = Executors.newSingleThreadScheduledExecutor();
        executor.scheduleAtFixedRate(this, 1, 1,
                TimeUnit.SECONDS);
    }

    @Override
    public void run() {
        long time = (System.currentTimeMillis() - start) / 1000;
        frame.setTitle(title + " (" + (time / 60) + ':' + (time % 60) + ")");
    }

    public void start() {
        start = System.currentTimeMillis();
    }

    public void exit() {
        executor.shutdown();
    }

}
